const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const path = require("path");

let location = __dirname;

const port = process.env.PORT || 8000;
let host = "localhost";
const server = http.createServer((req, res) => {
if(req.url==="/"){
  res.writeHead(200, { "Content-Type": "application/json" });
  res.write(`Welcome on my HTTP Server.
  Server is running good.
  You are on correct path.
  Thanks for visit.`);
  res.end();
}
  else if (req.url === "/html") {
    res.writeHead(200, { "Content-Type": "text/html" });
    fs.readFile(`${location}/HTML.html`, (err, data) => {
      if (err) {
        res.writeHead(404, { "Content-Type": "text/html" });
        res.write("Sorry can't read html file!");
        res.end();
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/json") {
    res.writeHead(200, { "Content-Type": "text/json" });
    fs.readFile(`${location}/JSON.json`, (err, data) => {
      if (err) {
        res.writeHead(404, { "Content-Type": "text/html" });
        res.write("Sorry can't read json file!");
        res.end();
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/uuid") {
    res.writeHead(200, { "Content-Type": "application/json" });
    const uuid = uuidv4();
    const response = { uuid };
    res.write(JSON.stringify(response));
    res.end();
  } else if (req.url.startsWith("/status/")) {
    const status_Code = req.url.split("/status/")[1];
    res.statusCode = status_Code;
    res.end(`Response with status code ${status_Code}`);
  } else if (req.url.startsWith("/delay/")) {
    const delayInSeconds = Number(req.url.split("/")[2]);
    if (isNaN(delayInSeconds) || delayInSeconds<0) {
      res.statusCode = 400;
      res.end("Invalid delay time");
      return;
    }

    setTimeout(() => {
      res.statusCode = 200;
      res.end(`Response after the delay of ${delayInSeconds} sec`);
    }, delayInSeconds * 1000);
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.write("404 Not Found");
    res.end();
  }
});

server.listen(port, host, () => {
  console.log(`listning on port 8000, you can get by http://${host}:${port}`);
});
